package org.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafSpringbootWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafSpringbootWebApplication.class, args);
	}

}
